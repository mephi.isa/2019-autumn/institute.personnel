class User:
	def __init__(self, login:str, password:str,role:str):
		self.login = login
		self.password = password
		self.role = role
	
	def set_login(self, login: str):
		self.login = login

	def get_login(self) -> str:
		return self.login
	def set_password(self, password: str):
		self.password = password

	def get_password(self) -> str:
		return self.password
	
	def set_role(self, role: str):
		self.role = role

	def get_role(self) -> str:
		return self.role
	
