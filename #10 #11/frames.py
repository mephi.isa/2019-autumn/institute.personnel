import datetime
import tkinter as tk
from tkinter import ttk
import psycopg2
from tkinter import messagebox
connection = psycopg2.connect(user = "postgres",
                                  password = "mamapapa1997",
                                  host = "127.0.0.1",
                                  port = "5432",
                                  database = "institute")
cursor = connection.cursor()
# Print PostgreSQL Connection properties
print (connection.get_dsn_parameters(),"\n")
 # Print PostgreSQL version


class GenericError(Exception):

    def __init__(self, message):
        super().__init__(message)

class Main(tk.Frame):
    def __init__(self,root):
        super().__init__(root)
        self.init_main()
    
    def init_main(self):
        self.login = tk.StringVar()
        self.password = tk.StringVar()

        self.label_1 = tk.Label(root, text = "Enter login: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(root, textvar = self.login, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(root, text = "Enter password: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(root, textvar = self.password,  show = "*" ,width = 30).place(x = 210, y = 80)

        self.btn_1 = tk.Button(root, text ="Sign in",width = "20",command= self.validate, background="green", foreground = "white", font = "15").place(x=150, y = 110)
    def validate(self):
        self.log=self.login.get()
        self.pas=self.password.get()
        cursor.execute("select * from users where login='"+self.log+"'")
        self.users=cursor.fetchone()
        print (self.users)
        if self.users and self.users[2] == self.pas:
            if self.users[3]=="manager":
                self.destroy()
                BranchList()
        else:
            messagebox.showinfo("Error", "Неверные данные")

class ContractFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Contracts")
        self.geometry("500x500+300+100")
        self.maindate = tk.StringVar()
        self.startdate = tk.StringVar()
        self.enddate = tk.StringVar()



        self.label_1 = tk.Label(self, text = "Enter contract date: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(self, textvar = self.maindate, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(self, text = "Enter start date: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(self, textvar = self.startdate ,width = 30).place(x = 210, y = 80)


        self.label_3 = tk.Label(self, text = "Enter exp date: ", fg = "black" , width = 30).place(x = 50, y = 110)
        self.entry_3 = ttk.Entry(self, textvar = self.enddate ,width = 30).place(x = 210, y = 110)

       
        self.btn_2 = tk.Button(self, text ="Create contract",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 140)
    def create(self):
        self.nowday=self.maindate.get()
        self.start=self.startdate.get()
        self.end=self.enddate.get()
        cursor.execute("insert into contract (contractDate, contractStartDate, contractEndDate) values('"+self.nowday+"','"+self.start+"','"+self.end+"')")
        connection.commit()
        self.destroy()
        BranchList()
        
   

class EmployeeFrame(tk.Toplevel):
    def __init__(self, brID: str):
        super().__init__()
        self.brID = brID
        self.init_main()

    def init_main(self):
        self.title("Employee")
        self.geometry("500x500+300+100")
        self.ln = tk.StringVar()
        self.firstn = tk.StringVar()
        self.middlen = tk.StringVar()
        self.bday = tk.StringVar()
        self.passports = tk.StringVar()
        self.passportn = tk.IntVar()
        self.scientifict = tk.StringVar()
        self.scienced = tk.StringVar()
        self.posit = tk.StringVar()
       
  


        self.label_1 = tk.Label(self, text = "Enter lastname: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(self, textvar = self.ln, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(self, text = "Enter firstname: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(self, textvar = self.firstn ,width = 30).place(x = 210, y = 80)


        self.label_3 = tk.Label(self, text = "Enter middlename: ", fg = "black" , width = 30).place(x = 50, y = 110)
        self.entry_3 = ttk.Entry(self, textvar = self.middlen ,width = 30).place(x = 210, y = 110)

        self.label_4 = tk.Label(self, text = "Enter birthday: ", fg = "black" , width = 30).place(x = 50, y = 140)
        self.entry_4 = ttk.Entry(self, textvar = self.bday, width = 30).place(x = 210, y = 140)
        
        self.label_5 = tk.Label(self, text = "Enter passport series: ", fg = "black" , width = 30).place(x = 50, y = 170)
        self.entry_5 = ttk.Entry(self, textvar = self.passports ,width = 30).place(x = 210, y = 170)


        self.label_6 = tk.Label(self, text = "Enter passport number: ", fg = "black" , width = 30).place(x = 50, y = 200)
        self.entry_6 = ttk.Entry(self, textvar = self.passportn ,width = 30).place(x = 210, y = 200)
       

        self.label_7 = tk.Label(self, text = "Enter scientifictitle: ", fg = "black" , width = 30).place(x = 50, y = 230)
        # self.entry_7 = ttk.Entry(self, textvar = self.scientifict, width = 30).place(x = 210, y = 230)
        cursor.execute('select * from title')
        record = cursor.fetchall()
        self.itemsT = []
        self.MAPT = {}
        for item in record:
            self.MAPT[item[1]] = item[0]
            self.itemsT.append(item[1])
        
      
        self.cb = ttk.Combobox(self, state="readonly" ,textvariable=self.scientifict, values = self.itemsT).place(x = 210, y = 230)
        # self.cb.set(items[0])

        self.label_8 = tk.Label(self, text = "Enter science degree ", fg = "black" , width = 30).place(x = 50, y = 260)
        # self.entry_8 = ttk.Entry(self, textvar = self.scienced ,width = 30).place(x = 210, y =260)
        cursor.execute('select * from degrees')
        record = cursor.fetchall()
        self.itemsd = []
        self.MAPD = {}
        for item in record:
            self.MAPD[item[1]] = item[0]
            self.itemsd.append(item[1])
        
      
        self.cb = ttk.Combobox(self, state="readonly" ,textvariable=self.scienced, values = self.itemsd).place(x = 210, y = 260)

        self.label_9 = tk.Label(self, text = "Enter position: ", fg = "black" , width = 30).place(x = 50, y = 290)
        # self.entry_9 = ttk.Entry(self, textvar = self.posit ,width = 30).place(x = 210, y = 290)
        cursor.execute('select * from position')
        record = cursor.fetchall()
        self.itemsp = []
        self.MAPP = {}
        for item in record:
            self.MAPP[item[1]] = item[0]
            self.itemsp.append(item[1])
        
      
        self.cb = ttk.Combobox(self, state="readonly" ,textvariable=self.posit, values = self.itemsp).place(x = 210, y = 290)
        # self.label_10= tk.Label(self, text = "Enter branch: ", fg = "black" , width = 30).place(x = 50, y = 310)
        # self.entry_10= ttk.Entry(self, textvar = self.branch ,width = 30).place(x = 210, y = 310)

        self.btn_2 = tk.Button(self, text ="Create employee",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 380)
    
    
    def create(self):
        l=self.ln.get()
        f=self.firstn.get()
        m=self.middlen.get()
        b=self.bday.get()
        ps=self.passports.get()
        pn=str(self.passportn.get())
        st=str(self.MAPT[self.scientifict.get()])
        print(st)
        sd=str(self.MAPD[self.scienced.get()])
        p=str(self.MAPP[self.posit.get()])
        br=str(self.brID)
        cursor.execute("insert into employee (lastname,firstname,middlename,birthday,passportseries,passportnumber,scienticid,scienticdegreeid,positionid,branchid) values('"+l+"','"+f+"','"+m+"','"+b+"','"+ps+"',"+pn+","+st+","+sd+","+p+","+br+")")
        connection.commit()
        self.destroy()
        ContractFrame()
        
class BranchFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Branch")
        self.geometry("500x500+300+100")
        self.nbranch = tk.StringVar()
        self.cbranch = tk.StringVar()


        self.label_1 = tk.Label(self, text = "Enter branch name: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(self, textvar = self.nbranch, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(self, text = "Enter branch city: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(self, textvar = self.cbranch ,width = 30).place(x = 210, y = 80)
            
        self.btn_3 = tk.Button(self, text ="Create branch",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 140)
    def create(self):
        n=self.nbranch.get()
        c=self.cbranch.get()
        cursor.execute("insert into branch (namebranch,citybranch) values('"+n+"','"+c+"')")
        connection.commit()

class BranchList(tk.Toplevel):
    """docstring for BranchList"""
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Branch")
        self.geometry("500x500+300+100")
        

        self.tree = ttk.Treeview(self, columns=('ID','Name', 'City'),height=15, show='headings')
        self.tree.column('ID', width=100)
        self.tree.column('Name', width=100)
        self.tree.column('City', width=100)

        self.tree.heading('ID', text = 'ID')
        self.tree.heading('Name', text = 'Name')
        self.tree.heading('City', text = 'City')
        cursor.execute('select * from branch')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0],record[i][1], record[i][2]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_3 = tk.Button(self, text ="Create new branch",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 400)
    def create(self):
        BranchFrame()
    def link_tree(self,event):
        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        print(id)
        EmployeeList(id)

class EmployeeList(tk.Toplevel):
    """docstring for List"""
    def __init__(self, brID:str):
        super().__init__()
        self.brID = str(brID)
        self.init_main()
       

    def init_main(self):
        self.title("Employee")
        self.geometry("500x500+300+100")
        self.tree = ttk.Treeview(self, columns=('ID','lastname','firstname','middlename','position'),height=15, show='headings')
        self.tree.column('ID', width=100)
        self.tree.column('lastname', width=100)
        self.tree.column('firstname', width=100)
        self.tree.column('middlename', width=100)
        self.tree.column('position', width=100)
        
        self.tree.heading('ID', text = 'ID')
        self.tree.heading('lastname', text = 'lastname')
        self.tree.heading('firstname', text = 'firstname')
        self.tree.heading('middlename', text = 'middlename')
        self.tree.heading('position', text = 'position')
        
        cursor.execute('select e.id, e.lastname, e.firstname,e.middlename, p.name from employee e left join position p on e.positionid = p.id where e.branchid = '+self.brID+'')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0],record[i][1], record[i][2], record[i][3], record[i][4]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()

        self.btn_3 = tk.Button(self, text ="Add employee",width = "20", command=self.add, background="green", foreground = "white", font = "15").place(x=150, y = 400)

    def link_tree(self,event):
        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        cursor.execute('update table employee set branchid = '+self.brID+' where id = '+id+'')
    
    def add(self):
        EmployeeFrame(self.brID)




        
if __name__ == "__main__":
    root = tk.Tk()
    root.title("Institute Personnel")
    root.geometry("500x500+300+100")
    app = Main(root)
    app.pack()
    root.mainloop()