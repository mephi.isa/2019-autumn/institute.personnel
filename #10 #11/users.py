from User import User
class Users (object):
	"""docstring fo Users """
	def __init__(self):
		self.users:list=[]
		admin = User("admin", "qwerty", "administrator")
		self.users.append(admin)

	def getUsers(self):
		return self.users

	def delUser(self,login:str):
		for i in range(len(self.users)):
			if self.users[i].get_login()== login:
				self.users.pop(i)
				return True
		return False

	def registrateUser(self, login:str, password:str, role:str):	
		user = User(login = login, password= password,role= role)
		self.users.append(user)

	def searchUsers(self, login:str):
		for user in self.users:
			if user.get_login()== login:
				return user
		return False