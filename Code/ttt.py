import unittest
from classes import Employee, Branch, Contract, EmployeeHistory, Archive
import datetime

class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.employee = Employee(
            id=1,
            lastname="Naubetov",
            firstname="Dastan",
            middlename="Nietovich",
            birthday=datetime.datetime(1997, 2, 12),
            passportseries="AA",
            passportnumber=123331,
            scientifictitle="Docent",
            sciencedegree="Ph.D",
            position="Dean"
        )

        self.branch = Branch(
            namebranch="SB",
            citybranch="Samara"
        )

        self.contract = Contract(
            contractid=2522,
            contractdate=datetime.datetime(2019, 5, 12),
            contractstartdate=datetime.datetime(2019, 6, 1),
            contractexpirationdate=datetime.datetime(2020, 5, 31)
        )

        self.employee.add_contract(self.contract)
        self.branch.add_employee(self.employee)
        self.employee_history = EmployeeHistory(
            employee=self.employee,
            date_archived=datetime.datetime.now())

    def test_contract_id(self):
        self.contract.set_contractid(366)
        self.assertEqual(self.contract.get_contractid(),366)

    def test_contract_date(self):
        self.contractdate.set_contractdate(datetime.datetime(2010, 10, 10))
        self.assertEqual(self.contract.get_contractdate(),datetime.datetime(2010, 10, 10))

    def test_contract_contractstartdate(self):
        self.contractstartdate.set_contractstartdate(datetime.datetime(2011, 10, 10))
        self.assertEqual(self.contract.get_contractstartdate(),datetime.datetime(2011, 10, 10))

    def test_contract_contractexpirationdate(self):
        self.contractexpirationdate.set_contractexpirationdate(datetime.datetime(2012, 10, 10))
        self.assertEqual(self.contract.get_contractexpirationdate(),datetime.datetime(2012, 10, 10))

    def test_employee_id (self):
        self.employee.set_id(366)
        self.assertEqual(self.employee.get_id(),366)

    def test_employee_birthday(self):
        self.birthday.set_birthday(datetime.datetime(1977, 10, 10))
        self.assertEqual(self.employee.get_birthday(),datetime.datetime(1977, 10, 10))

    def test_employee_lastname (self):
        self.employee.set_lastname("Marratov")
        self.assertEqual(self.employee.get_lastname(),"Marratov")

    def test_employee_firstname (self):
        self.employee.set_firstname("Anvar")
        self.assertEqual(self.employee.get_firstname(),"Anvar")

    def test_employee_middlename (self):
        self.employee.set_middlename("Muratovich")
        self.assertEqual(self.employee.get_middlename(),"Muratovich")

    def test_employee_passportseries (self):
        self.employee.set_passportseries("QA")
        self.assertEqual(self.employee.get_passportseries(),"QA")

    def test_employee_passportnumber (self):
        self.employee.set_passportnumber(3123166)
        self.assertEqual(self.employee.get_passportnumber(), 3123166)

    def test_employee_scientifictitle(self):
        self.employee.set_scientifictitle("professor")
        self.assertEqual(self.employee.get_scientifictitle(), "professor")

    def test_employee_sciencedegree(self):
        self.employee.set_sciencedegree("Doctor of Technical Sciences")
        self.assertEqual(self.employee.get_sciencedegree(), "Doctor of Technical Sciences")

    def test_employee_position(self):
        self.employee.set_position("Head of department")
        self.assertEqual(self.employee.get_position(), "Head of department")

    def test_branch_namebranch(self):
        self.branch.set_namebranch(12)
        self.assertEqual(self.branch.get_namebranch(),12)

    def test_branch_citybranch(self):
        self.branch.set_citybranch("TB")
        self.assertEqual(self.branch.get_citybranch(), "TB")

    def test_contract_add(self):
        self.employee.add_contract(Contract(
            contractid=221456485,
            contractdate=datetime.datetime(2019, 10, 12),
            contractstartdate=datetime.datetime(2019, 6, 1),
            contractexpirationdate=datetime.datetime(2020, 5, 31)
        ))
        self.assertEqual(len(self.employee.get_contracts()), 2)

    def test_find_contract(self):
        self.assertIsNone(self.employee.find_contract(contractid=221456485))

    def test_add_employee(self):
        self.branch.add_employee(Employee(
            id=5,
            lastname="Sdfvbghg",
            firstname="Bfdfvcx",
            middlename="Nigfgffdich",
            birthday=datetime.datetime(1997, 10, 12),
            passportseries="AA",
            passportnumber=123331,
            scientifictitle="Docent",
            sciencedegree="Ph.D",
            position="Dean"
        ))
        self.assertEqual(len(self.branch.get_employee()),2)

    def test_find_employee(self):
        self.assertIsNone(self.branch.find_employee(id=1))

if __name__ == '__main__':
    unittest.main()
# python -m unittest testyperson.py
