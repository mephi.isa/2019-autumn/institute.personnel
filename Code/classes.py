import datetime


class GenericError(Exception):

    def __init__(self, message):
        super().__init__(message)


class Contract:
    def __init__(self, contractid: int, contractdate: datetime.datetime, contractstartdate: datetime.datetime,
                 contractexpirationdate: datetime.datetime):
        self.contractid: int = contractid
        self.contractdate: datetime = contractdate
        self.contractstartdate: datetime = contractstartdate
        self.contractexpirationdate: datetime = contractexpirationdate

    def set_contractid(self, contractid: int):
        self.contractid = contractid

    def get_contractid(self) -> int:
        return self.contractid

    def set_contractdate(self, contractdate: datetime):
        self.contractdate = contractdate

    def get_contractdate(self) -> datetime:
        return self.contractdate

    def set_contractstartdate(self, contractstartdate: datetime):
        self.contractstartdate = contractstartdate

    def get_contractstartdate(self) -> datetime:
        return self.contractstartdate

    def set_contractexpirationdate(self, contractexpirationdate: datetime):
        self.contractexpirationdate = contractexpirationdate

    def get_contractexpirationdate(self) -> datetime:
        def find_contract(self, **kwargs):
         out_contract: Contract = None
        if "contractid" in kwargs:
            for i, contract in enumerate(self.contracts):
                if contract.get_contractid() == kwargs["contractid"]:
                    out_contract = contract
                    break
                    return out_contract

class Employee:

    def __init__(self, lastname: str, firstname: str, middlename: str, birthday: datetime.datetime, passportseries: str,
                 passportnumber: int, id: int, scientifictitle: str, sciencedegree: str, position: str):
        self.lastname: str = lastname
        self.firstname: str = firstname
        self.middlename: str = middlename
        self.birthday: datetime = birthday
        self.passportseries: str = passportseries
        self.passportnumber: int = passportnumber
        self.id: int = id
        self.scientifictitle: str = scientifictitle
        self.sciencedegree: str = sciencedegree
        self.position: str = position
        self.contracts: list = []

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_birthday(self, birthday: datetime):
        self.birthday = birthday

    def get_birthday(self) -> datetime:
        return self.birthday

    def set_passportseries(self, passportseries: str):
        self.passportseries = passportseries

    def get_passportseries(self) -> str:
        return self.passportseries

    def set_passportnumber(self, passportnumber: int):
        self.passportnumber = passportnumber

    def get_passportnumber(self) -> int:
        return self.passportnumber

    def set_lastname(self, lastname: str):
        self.lastname = lastname

    def get_lastname(self) -> str:
        return self.lastname

    def set_firstname(self, firstname: str):
        self.firstname = firstname

    def get_firstname(self) -> str:
        return self.firstname

    def set_middlename(self, middlename: str):
        self.mimiddlename = middlename

    def get_middlename(self) -> str:
        return self.middlename

    def set_scientifictitle(self, scientifictitle: str):
        self.scientifictitle = scientifictitle

    def get_scientifictitle(self) -> str:
        return self.scientifictitle

    def set_sciencedegree(self, sciencedegree: str):
        self.sciencedegree = sciencedegree

    def get_sciencedegree(self) -> str:
        return self.sciencedegree

    def set_position(self, position: str):
        self.position = position

    def get_position(self) -> str:
        return self.position

    def get_contracts(self) -> list:
        return self.contracts

    def add_contract(self, contract: Contract):
        self.contracts.append(contract)

    def remove_contract(self, contract: Contract):
        self.contracts.remove(contract)

    def find_contract(self, **kwargs):
        out_contract: Contract = None
        if "contractid" in kwargs:
            for i, contract in enumerate(self.contracts):
                if contract.get_contractid() == kwargs["contractid"]:
                    out_contract = contract
                    break
                return out_contract

class Branch:
    def __init__(self, namebranch: str, citybranch: str):
        self.namebranch: str = namebranch
        self.citybranch: str = citybranch
        self.employees: Employee = []

    def set_namebranch(self, namebranch: str):
        self.namebranch = namebranch

    def get_namebranch(self) -> str:
        return self.namebranch

    def set_citybranch(self, citybranch: str):
        self.citybranch = citybranch

    def get_citybranch(self) -> str:
        return self.citybranch

    def get_employee(self) -> list:
        return self.employees

    def add_employee(self, employee: Employee):
        self.employees.append(employee)

    def remove_employee(self, employee: Employee):
        self.employees.remove(employee)

    def find_employee(self, **kwargs):
        out_employee: Employee = None
        if "id" in kwargs:
            for i, employee in enumerate(self.employees):
                if employee.get_id() == id:
                    out_employee = employee
                    break
        return out_employee


class EmployeeHistory(Employee):

    def __init__(self, employee: Employee, date_archived):
        super().__init__(
            lastname=employee.lastname,
            firstname=employee.firstname,
            middlename=employee.middlename,
            id=employee.id,
            birthday=employee.birthday,
            passportseries=employee.passportseries,
            passportnumber=employee.passportnumber,
            scientifictitle=employee.scientifictitle,
            sciencedegree=employee.sciencedegree,
            position=employee.position

        )
        self.date_archived: datetime = date_archived

    def set_date_archived(self, date_archived: datetime):
        self.date_archived = date_archived

    def get_date_archived(self) -> datetime:
        return self.date_archived


class Archive:

    def __init__(self):
        self.employee_history: list = []

    def add_employee(self, employee: Employee):
        self.employee_history.append(EmployeeHistoryy(employee=employee,
                                                      date_archived=datetime.datetime.now().replace(hour=0, minute=0,
                                                                                                    second=0,
                                                                                                    microsecond=0)))

    def find_archived_employee(self, **kwargs) -> EmployeeHistory:
        out_employee: EmployeeHistory = None
        if "id" in kwargs and "date_archived" in kwargs:
            for i, employee in enumerate(self.employees_history):
                if employee.get_id() == kwargs["id"] and employee.get_date_archived() == kwargs["date_archived"]:
                    out_employee = employee
                    break
        elif "id" in kwargs:
            for i, employee in enumerate(self.employees_history):
                if employee.get_id() == kwargs["id"]:
                    if out_employee == None:
                        out_employee = employee
                    elif out_employee.get_date_archived() < employee.get_date_archived():
                        out_employee = employee
        else:
            raise GenericError("employee_id arguments required")
        return out_employee