import datetime
import tkinter as tk
from tkinter import ttk
import psycopg2
from PIL import ImageTk, Image

from datetime import *
from tkinter import messagebox
connection = psycopg2.connect(user = "postgres",
                                  password = "mamapapa1997",
                                  host = "127.0.0.1",
                                  port = "5432",
                                  database = "institute")
cursor = connection.cursor()
# Print PostgreSQL Connection properties
print (connection.get_dsn_parameters(),"\n")
 # Print PostgreSQL version


class GenericError(Exception):

    def __init__(self, message):
        super().__init__(message)

class Main(tk.Frame):
    def __init__(self,root):
        super().__init__(root)
        self.init_main()
    
    def init_main(self):
        self.login = tk.StringVar()
        self.password = tk.StringVar()

        self.label_1 = tk.Label(root, text = "Enter login: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(root, textvar = self.login, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(root, text = "Enter password: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(root, textvar = self.password,  show = "*" ,width = 30).place(x = 210, y = 80)

        self.btn_1 = tk.Button(root, text ="Sign in",width = "20",command= self.validate, background="green", foreground = "white", font = "15").place(x=150, y = 110)
    def validate(self):
        self.log=self.login.get()
        self.pas=self.password.get()
        cursor.execute("select * from users where login='"+self.log+"'")
        self.users=cursor.fetchone()
        print (self.users)
        if self.users and self.users[2] == self.pas and self.users[3]=="manager":
            # root.destroy()
            BranchList()
            # Notification()
        elif self.users and self.users[2] == self.pas and self.users[3]=="admin":
            # root.destroy()
            AdminList()
        else:
            messagebox.showinfo("Error", "Неверные данные")

class AdminFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Admin Panel")
        self.geometry("500x500+300+100")
        self.llogin = tk.StringVar()
        self.ppaswword = tk.StringVar()
        self.rrole = tk.StringVar()


        self.label_66 = tk.Label(self, text = "Login: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_66 = ttk.Entry(self, textvar = self.llogin, width = 30).place(x = 210, y = 50)
        
        self.label_26 = tk.Label(self, text = "Password: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_26 = ttk.Entry(self, textvar = self.ppaswword ,width = 30).place(x = 210, y = 80)
        
        self.label_77 = tk.Label(self, text = "Role: ", fg = "black" , width = 30).place(x = 50, y = 110)
        self.entry_77 = ttk.Entry(self, textvar = self.rrole ,width = 30).place(x = 210, y = 110)   
        self.btn_3 = tk.Button(self, text ="Create new user",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 140)
    def create(self):
        l=self.llogin.get()
        p=self.ppaswword.get()
        r=self.rrole.get()
        cursor.execute("insert into users (login,password,role) values('"+l+"','"+p+"','"+r+"')")
        connection.commit()
        self.destroy()
        AdminList()

class NotificationList(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Уведомление о контракте")
        self.geometry("500x500+300+100")
        cursor.execute("select c.id, c.contractenddate, e.firstname, e.lastname from contract c left join employee e on c.userid=e.id")
        record = cursor.fetchall()
        print(record)
        today = date.today()
        print(date.today() + timedelta(days=10))
        self.tree = ttk.Treeview(self, columns=('Notification', "Name" , "Lastname"),height=15, show='headings')
        self.tree.column('Notification', width=200)
        self.tree.column('Name', width=100)
        self.tree.column('Lastname', width=100)
        self.tree.heading("Notification", text ='Уведомление')
        self.tree.heading("Name", text ="Имя")
        self.tree.heading("Lastname", text ="Фамилия")
        for item in record:
            if today - timedelta(days=10) > datetime.strptime(item[1], '%Y-%m-%d').date():
                cursor.execute("delete from contract where id = "+str(item[0])+"")
                connection.commit()
                self.tree.insert('', 'end', values=("Контракт-удален-ID:-"+ str(item[0])+"", str(item[2]), str(item[3]) ))
            elif  datetime.strptime(item[1], '%Y-%m-%d').date() <= today:
                self.tree.insert('', 'end', values=("Контракт-истекает-ID:-"+ str(item[0])+"", str(item[2]), str(item[3]) ))
        self.tree.pack()

class AdminList(tk.Toplevel):
    """docstring for AdminList"""
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("AdminList")
        self.geometry("500x500+300+100")
        

        self.tree = ttk.Treeview(self, columns=('login','Password', 'Role'),height=15, show='headings')
        self.tree.column('login', width=100)
        self.tree.column('Password', width=100)
        self.tree.column('Role', width=100)

        self.tree.heading('login', text = 'login')
        self.tree.heading('Password', text = 'Password')
        self.tree.heading('Role', text = 'Role')
        cursor.execute('select * from users')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0],record[i][1], record[i][2]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_66 = tk.Button(self, text ="Create new user",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 400)
    def create(self):
        self.destroy()
        AdminFrame()

    def link_tree(self,event):
        tl = tk.Toplevel()
        tl.title("Select")
        tl.geometry("500x200+300+300")

        btn_66 = tk.Button(tl, text ="Update",width = "20", command=self.create, background="green", foreground = "white", font = "15").pack()
        btn_67 = tk.Button(tl, text ="Delete",width = "20", command=self.create, background="green", foreground = "white", font = "15").pack()

        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        print(id)
        # EmployeeList(id)

class ContractFrame(tk.Toplevel):
    def __init__(self, id):
        super().__init__()
        self.id =id
        self.init_main()

    def init_main(self):
        self.title("Contracts")
        self.geometry("500x500+300+100")
        self.maindate = tk.StringVar()
        self.mainmonth = tk.StringVar()
        self.mainyear = tk.StringVar()
        self.startdate = tk.StringVar()
        self.startmonth = tk.StringVar()
        self.startyear = tk.StringVar()
        self.enddate = tk.StringVar()
        self.endmonth = tk.StringVar()
        self.endyear = tk.StringVar()



        self.label_1 = tk.Label(self, text = "Enter contract date: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_1 = tk.Entry(self, width=5,  textvar = self.maindate ).place(x=210, y=50)
        self.label_1 = tk.Label(self, text='/' ).place(x=230, y=50)
        self.entry_1 = tk.Entry(self, width=5,  textvar = self.mainmonth).place(x=240, y=50)
        self.label_1 = tk.Label(self, text='/').place(x=260, y=50)
        self.entry_1 = tk.Entry(self, width=10,  textvar = self.mainyear).place(x=270, y=50)

        
        self.label_2 = tk.Label(self, text = "Enter start date: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = tk.Entry(self, width=5,  textvar = self.startdate ).place(x=210, y=80)
        self.label_2 = tk.Label(self, text='/' ).place(x=230, y=80)
        self.entry_2 = tk.Entry(self, width=5,  textvar = self.startmonth).place(x=240, y=80)
        self.label_2 = tk.Label(self, text='/').place(x=260, y=80)
        self.entry_2 = tk.Entry(self, width=10,  textvar = self.startyear).place(x=270, y=80)


        self.label_3 = tk.Label(self, text = "Enter exp date: ", fg = "black" , width = 30).place(x = 50, y = 110)
        self.entry_3 = tk.Entry(self, width=5,  textvar = self.enddate ).place(x=210, y=110)
        self.label_3 = tk.Label(self, text='/' ).place(x=230, y=110)
        self.entry_3 = tk.Entry(self, width=5,  textvar = self.endmonth).place(x=240, y=110)
        self.label_3 = tk.Label(self, text='/').place(x=260, y=110)
        self.entry_3 = tk.Entry(self, width=10,  textvar = self.endyear).place(x=270, y=110)
       
        self.btn_2 = tk.Button(self, text ="Create contract",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 140)
    def create(self):
        today = date.today()
        self.nowday=self.maindate.get() + "/" + self.mainmonth.get() + "/" + self.mainyear.get() 
        self.start=self.startdate.get() + "/" + self.startmonth.get() + "/" + self.startyear.get()
        self.end=self.enddate.get() + "/" + self.endmonth.get() + "/" + self.endyear.get()
        print( datetime.strptime(self.nowday, '%d/%m/%Y').date() , today)
        if datetime.strptime(self.nowday, '%d/%m/%Y').date() < today or datetime.strptime(self.start, '%d/%m/%Y').date() < today or  datetime.strptime(self.end, '%d/%m/%Y').date() < today :
            messagebox.showinfo("Error","Невозможно создать контракт задним числом")
        else:
            cursor.execute("insert into contract (contractDate, contractStartDate, contractEndDate, userid) values(TO_DATE('"+self.nowday+"', 'DD/MM/YYYY'),TO_DATE('"+self.start+"', 'DD/MM/YYYY'),TO_DATE('"+self.end+"', 'DD/MM/YYYY'), "+str(self.id)+")")
            connection.commit()
            self.destroy()
            BranchList()
        
   

class EmployeeFrame(tk.Toplevel):
    def __init__(self, brID: str):
        super().__init__()
        self.brID = brID
        self.init_main()

    def init_main(self):
        self.title("Employee")
        self.geometry("500x500+300+100")
        self.ln = tk.StringVar()
        self.firstn = tk.StringVar()
        self.middlen = tk.StringVar()
        self.bday = tk.StringVar()
        self.passports = tk.StringVar()
        self.passportn = tk.IntVar()
        self.scientifict = tk.StringVar()
        self.scienced = tk.StringVar()
        self.posit = tk.StringVar()
       
  


        self.label_1 = tk.Label(self, text = "Enter lastname: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(self, textvar = self.ln, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(self, text = "Enter firstname: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(self, textvar = self.firstn ,width = 30).place(x = 210, y = 80)


        self.label_3 = tk.Label(self, text = "Enter middlename: ", fg = "black" , width = 30).place(x = 50, y = 110)
        self.entry_3 = ttk.Entry(self, textvar = self.middlen ,width = 30).place(x = 210, y = 110)

        self.label_4 = tk.Label(self, text = "Enter birthday: ", fg = "black" , width = 30).place(x = 50, y = 140)
        self.entry_4 = ttk.Entry(self, textvar = self.bday, width = 30).place(x = 210, y = 140)
        
        self.label_5 = tk.Label(self, text = "Enter passport series: ", fg = "black" , width = 30).place(x = 50, y = 170)
        self.entry_5 = ttk.Entry(self, textvar = self.passports ,width = 30).place(x = 210, y = 170)


        self.label_6 = tk.Label(self, text = "Enter passport number: ", fg = "black" , width = 30).place(x = 50, y = 200)
        self.entry_6 = ttk.Entry(self, textvar = self.passportn ,width = 30).place(x = 210, y = 200)
       

        self.label_7 = tk.Label(self, text = "Enter scientifictitle: ", fg = "black" , width = 30).place(x = 50, y = 230)
        # self.entry_7 = ttk.Entry(self, textvar = self.scientifict, width = 30).place(x = 210, y = 230)
        cursor.execute('select * from title')
        record = cursor.fetchall()
        self.itemsT = []
        self.MAPT = {}
        for item in record:
            self.MAPT[item[1]] = item[0]
            self.itemsT.append(item[1])
        
      
        self.cb = ttk.Combobox(self, state="readonly" ,textvariable=self.scientifict, values = self.itemsT).place(x = 210, y = 230)
        # self.cb.set(items[0])

        self.label_8 = tk.Label(self, text = "Enter science degree ", fg = "black" , width = 30).place(x = 50, y = 260)
        # self.entry_8 = ttk.Entry(self, textvar = self.scienced ,width = 30).place(x = 210, y =260)
        cursor.execute('select * from degrees')
        record = cursor.fetchall()
        self.itemsd = []
        self.MAPD = {}
        for item in record:
            self.MAPD[item[1]] = item[0]
            self.itemsd.append(item[1])
        
      
        self.cb = ttk.Combobox(self, state="readonly" ,textvariable=self.scienced, values = self.itemsd).place(x = 210, y = 260)

        self.label_9 = tk.Label(self, text = "Enter position: ", fg = "black" , width = 30).place(x = 50, y = 290)
        # self.entry_9 = ttk.Entry(self, textvar = self.posit ,width = 30).place(x = 210, y = 290)
        cursor.execute('select * from position')
        record = cursor.fetchall()
        self.itemsp = []
        self.MAPP = {}
        for item in record:
            self.MAPP[item[1]] = item[0]
            self.itemsp.append(item[1])
        
      
        self.cb = ttk.Combobox(self, state="readonly" ,textvariable=self.posit, values = self.itemsp).place(x = 210, y = 290)
        # self.label_10= tk.Label(self, text = "Enter branch: ", fg = "black" , width = 30).place(x = 50, y = 310)
        # self.entry_10= ttk.Entry(self, textvar = self.branch ,width = 30).place(x = 210, y = 310)

        self.btn_2 = tk.Button(self, text ="Create employee",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 380)
    
    
    def create(self):
        l=self.ln.get()
        f=self.firstn.get()
        m=self.middlen.get()
        b=self.bday.get()
        ps=self.passports.get()
        pn=str(self.passportn.get())
        st=str(self.MAPT[self.scientifict.get()])
        print(st)
        sd=str(self.MAPD[self.scienced.get()])
        p=str(self.MAPP[self.posit.get()])
        br=str(self.brID)
        cursor.execute("insert into employee (lastname,firstname,middlename,birthday,passportseries,passportnumber,scienticid,scienticdegreeid,positionid,branchid) values('"+l+"','"+f+"','"+m+"','"+b+"','"+ps+"',"+pn+","+st+","+sd+","+p+","+br+")")
        connection.commit()
        cursor.execute("select id from employee where passportnumber = "+pn+"")
        record = cursor.fetchone()
        self.destroy()
        print(record)
        ContractFrame(record[0])
        
class BranchFrame(tk.Toplevel):
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Branch")
        self.geometry("500x500+300+100")
        self.nbranch = tk.StringVar()
        self.cbranch = tk.StringVar()


        self.label_1 = tk.Label(self, text = "Enter branch name: ", fg = "black" , width = 30).place(x = 50, y = 50)
        self.entry_2 = ttk.Entry(self, textvar = self.nbranch, width = 30).place(x = 210, y = 50)
        
        self.label_2 = tk.Label(self, text = "Enter branch city: ", fg = "black" , width = 30).place(x = 50, y = 80)
        self.entry_2 = ttk.Entry(self, textvar = self.cbranch ,width = 30).place(x = 210, y = 80)
            
        self.btn_3 = tk.Button(self, text ="Create branch",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 140)
    def create(self):
        n=self.nbranch.get()
        c=self.cbranch.get()
        cursor.execute("insert into branch (namebranch,citybranch) values('"+n+"','"+c+"')")
        connection.commit()
        self.destroy()
        BranchList()

class BranchList(tk.Toplevel):
    """docstring for BranchList"""
    def __init__(self):
        super().__init__()
        self.init_main()

    def init_main(self):
        self.title("Branch")
        self.geometry("500x500+300+100")
        

        self.tree = ttk.Treeview(self, columns=('ID','Name', 'City'),height=15, show='headings')
        self.tree.column('ID', width=100)
        self.tree.column('Name', width=100)
        self.tree.column('City', width=100)

        self.tree.heading('ID', text = 'ID')
        self.tree.heading('Name', text = 'Name')
        self.tree.heading('City', text = 'City')
        cursor.execute('select * from branch')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0],record[i][1], record[i][2]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()
        self.btn_3 = tk.Button(self, text ="Create new branch",width = "20", command=self.create, background="green", foreground = "white", font = "15").place(x=150, y = 400)
        self.btn_4 = tk.Button(self, text ="Уведомление",width = "20", command=self.create2, background="green", foreground = "white", font = "15").place(x=150, y = 430)  
    def create(self):
        self.destroy()
        BranchFrame()
    def create2(self):
        NotificationList()
    def link_tree(self,event):
        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        print(id)
        self.destroy()
        EmployeeList(id)

class EmployeeList(tk.Toplevel):
    """docstring for List"""
    def __init__(self, brID:str):
        super().__init__()
        self.brID = str(brID)
        self.init_main()
       

    def init_main(self):
        self.title("Employee")
        self.geometry("500x500+300+100")
        self.tree = ttk.Treeview(self, columns=('ID','lastname','firstname','middlename','position'),height=15, show='headings')
        self.tree.column('ID', width=100)
        self.tree.column('lastname', width=100)
        self.tree.column('firstname', width=100)
        self.tree.column('middlename', width=100)
        self.tree.column('position', width=100)
        
        self.tree.heading('ID', text = 'ID')
        self.tree.heading('lastname', text = 'lastname')
        self.tree.heading('firstname', text = 'firstname')
        self.tree.heading('middlename', text = 'middlename')
        self.tree.heading('position', text = 'position')
        
        cursor.execute('select e.id, e.lastname, e.firstname,e.middlename, p.name from employee e left join position p on e.positionid = p.id where e.branchid = '+self.brID+'')
        record = cursor.fetchall()
        for i in range(len(record)):
            self.tree.insert('', 'end', values=(record[i][0],record[i][1], record[i][2], record[i][3], record[i][4]))
        self.tree.bind("<Double-1>", self.link_tree)
        self.tree.pack()

        self.btn_3 = tk.Button(self, text ="Add employee",width = "20", command=self.add, background="green", foreground = "white", font = "15").place(x=150, y = 400)

    def link_tree(self,event):
        curItem = self.tree.focus()
        id = self.tree.item(curItem)['values'][0]
        cursor.execute('select * from contract where userid = '+str(id)+'')
        exist = cursor.fetchone()
        if exist:
            print(exist)
            messagebox.showinfo("Информация", "День начала: "+str(exist[2])+"\n День завершения: "+str(exist[3])+" ")
        else:
            self.destroy()
            ContractFrame(id)
    def add(self):
        self.destroy()
        EmployeeFrame(self.brID)





        
if __name__ == "__main__":
    root = tk.Tk()
    root.title("Institute Personnel")
    root.geometry("500x500+300+100")
    IMAGE_PATH = '123.jpg'
    WIDTH, HEIGTH = 800, 800
    canvas = tk.Canvas(root, width=WIDTH, height=HEIGTH)
    img = ImageTk.PhotoImage(Image.open(IMAGE_PATH).resize((WIDTH, HEIGTH), Image.ANTIALIAS))
    canvas.background = img  # Keep a reference in case this code is put in a function.
    bg = canvas.create_image(0, 0, anchor=tk.NW, image=img)
    app = Main(root)
    canvas.pack()
    app.pack()
    root.mainloop()

a = input("Press Enter")
