### Рисунки и диаграммы в формате Draw.io

Чтобы обновить PNG-файлы (используются в документах Markdown), используйте
```bash
make DRAWIO=path/to/your/drawio
```

Приложение Drawio desktop может быть загружено с [Github](https://github.com/jgraph/drawio-desktop/releases).
