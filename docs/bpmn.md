# Модель бизнес-процессов

![Авторизация пользователя](drawio/.img/BPMN_Auth.png)
![Добавление пользователя](drawio/.img/BPMN_UserAdd.png)
![Изменение данных пользователя пользователя](drawio/.img/BPMN_UserEdit.png)
![Удаление пользователя](drawio/.img/BPMN_UserDel.png)
![Получение информации о сотруднике](drawio/.img/BPMN_GetInfo.png)
![Изменение информации о сотруднике](drawio/.img/BPMN_EditInfo.png)
![Добавление нового сотрудника](drawio/.img/BPMN_AddNewInfo.png)
![Уведомление об истечении контракта](drawio/.img/BPMN_ContractNotify.png)
![Генерация приказа об увольнении](drawio/.img/BPMN_ContractTerminate.png)
![Архивация информации](drawio/.img/BPMN_MoveToArchive.png)

